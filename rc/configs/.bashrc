if [ -e /etc/bashrc ]; then
  source /etc/bashrc
fi

export HISTCONTROL=erasedups
export HISTSIZE=100000
shopt -s histappend

if [ "$FIRSTENVLOAD" != "true" ]; then
  export FIRSTENVLOAD=true
  export ORIGINAL_PATH=~/bin:.:$PATH:/usr/local/bin:/opt/local/bin
  export ORIGINAL_DYLD_LIBRARY_PATH=.:$DYLD_LIBRARY_PATH
  export CMAKE_BUILD_TYPE=Debug
  export ENABLE_ASSERTIONS=ON
fi

if [ -d ~/.local/lib ]; then
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/.local/lib
fi

export ZER0RC_ROOT=~/.zer0rc

uname -a  2>&1 | grep iPhone && export DEVICEMODE=true

if [ "$DEVICEMODE" == true ]; then
  if [ `hostname` == "iPhone" ]; then
    echo "This is a new device, pick a nice name from Moby Dick: "
    read hostname
    scutil --set HostName $hostname
  fi
  export IPHONEMSG="THIS IS AN IPHONE..."
fi

# Use macvim if available
if [ -e /Applications/MacVim/MacVim.app/Contents/MacOS/Vim ]; then
  alias vim="/Applications/MacVim/MacVim.app/Contents/MacOS/Vim"
elif [ -e /Applications/MacVim.app/Contents/MacOS/Vim ]; then
  alias vim="/Applications/MacVim.app/Contents/MacOS/Vim"
fi

export TERM=xterm-256color
export LSCOLORS="gxFxcxdxCxEgedabagacad"
export PS1LRED="\[\e[01;31m\]"
export PS1LGREEN="\[\e[01;32m\]"
export PS1LCYAN="\[\e[01;36m\]"
export PS1WHITE="\[\e[01;37m\]"
export PS1BLACK="\[\e[01;00m\]"
export LSCOLORS="gxFxcxdxCxEgedabagacad"

function reloadenv {
  source $ENVCONFIGFILE
  echo "Reloaded environment. Have fun."
}

function nocolors {
  if [ "$NOCOLORS" != "true" ]; then
    export NOCOLORS=true
    export PS1LRED=""
    export PS1LGREEN=""
    export PS1LCYAN=""
    export PS1WHITE=""
    export PS1BLACK=""
    reloadenv
  fi
}

function white_mode {
  export TERM_PROGRAM=Apple_Terminal
  reloadenv
}

if [ "$XTERM_VERSION" != "" ]; then
  nocolors
fi

if [ "$NOCOLORS" != "true" ]; then
  export PS1LRED="\[\e[01;31m\]"
  export PS1LGREEN="\[\e[01;32m\]"
  export PS1LCYAN="\[\e[01;36m\]"
  export PS1WHITE="\[\e[01;37m\]"
  export PS1BLACK="\[\e[01;00m\]"
fi

export ENDPROMPT=">"
whoami | grep root && export ENDPROMPT="#"


alias __git_ps1="git branch 2>&1 | grep '*' | cut -d' ' -f2"
export PS1_0="$PS1LCYAN[ $PS1LRED\! $PS1WHITE- \d \@ $PS1WHITE- $PS1LRED\j jobs $PS1WHITE- \u@\h:$PS1LRED\$ $PS1WHITE$cwp/$CMAKE_BUILD_TYPE $PS1LRED\${?##0} \$(__git_ps1) $PS1WHITE$IPHONEMSG$PS1LCYAN]"
export PS1_1="$PS1LGREEN\w"
export PS1_2="$PS1WHITE"
if [ "$TERM_PROGRAM" == "Apple_Terminal" ]; then
  export PS1_0="$PS1LCYAN[ $PS1LRED\! $PS1BLACK- \d \@ $PS1BLACK- $PS1LRED\j jobs $PS1BLACK- \u@\h:$PS1LRED\$ $PS1BLACK$cwp/$CMAKE_BUILD_TYPE $PS1LRED\${?##0} \$(__git_ps1) $PS1BLACK$IPHONEMSG$PS1LCYAN]"
  export PS1_2="$PS1BLACK"
fi

export PS1="\n$PS1_0\n$PS1_1\n$PS1_2$ENDPROMPT "

alias r=reset
alias j=jobs
alias ls="ls -GCF"
alias ll="ls -lhGF"
alias la="ls -lhaGF"
alias ctags="/usr/local/bin/ctags -R --c++-kinds=+p --fields=+iaS --extra=+q"

export EDITOR=vim
export VISUAL=$EDITOR

export PROJECT_DIR=~/Projects
export BUILD_DIR=~/BUILDS
export TMP_DIR=~/tmp
export ENVCONFIGFILE=~/.bashrc

# export DYLD_LIBRARY_PATH=$ORIGINAL_DYLD_LIBRARY_PATH
export PATH=$ORIGINAL_PATH

function UpdatePathWithWSBin {
  if [ "$1" == "" ]; then
    if [ "$CMAKE_USE_ASAN" == "true" ]; then
      export WS_DIR=$BUILD_DIR/$cwp'_build'/'ASAN_'$CMAKE_BUILD_TYPE
    else
      export WS_DIR=$BUILD_DIR/$cwp'_build'/$CMAKE_BUILD_TYPE
    fi
  else
    export WSBIN_DIR=$1
  fi

  export WSBIN_DIR=$WS_DIR/bin
  export PATH=$WS_DIR:$WSBIN_DIR:$ORIGINAL_PATH
}

if [ "$cwp" != "" ]; then
  UpdatePathWithWSBin
fi

mkdir -p $PROJECT_DIR
mkdir -p $BUILD_DIR
mkdir -p $TMP_DIR

export BUILD_SYS_TYPE="Ninja"
export BUILD_SYS_CMD=ninja

function blow {
  ssh -C -c blowfish $1
}

function gdstage {
  if [ "$1" != "" ]; then
    git diff $1 origin/`git branch | grep "\*" | cut -f2 -d' '`
  else
    git diff origin/`git branch | grep "\*" | cut -f2 -d' '`
  fi
}

function listws {
  \ls $PROJECT_DIR | cut -f1 -d'_'
}

function toggle_flavor {
  if [ "$CMAKE_BUILD_TYPE" == "Debug" ]; then
    export CMAKE_BUILD_TYPE=Release
    export ENABLE_ASSERTIONS=OFF
  elif [ "$CMAKE_BUILD_TYPE" == "Release" ]; then
    export CMAKE_BUILD_TYPE=RelWithDebInfo
    export ENABLE_ASSERTIONS=ON
  else
    export CMAKE_BUILD_TYPE=Debug
    export ENABLE_ASSERTIONS=ON
  fi
  echo "CMAKE_BUILD_TYPE is now $CMAKE_BUILD_TYPE"
  UpdatePathWithWSBin
  reloadenv
  ls $WSBIN_DIR &> /dev/null && echo "There are available bits at $WSBIN_DIR..."
}

function toggle_asan {
  if [ "$CMAKE_USE_ASAN" == "true" ]; then
    export CMAKE_USE_ASAN=false
    echo "ASAN is now disabled."
  else
    export CMAKE_USE_ASAN=true
    echo "ASAN is now enabled."
  fi
  UpdatePathWithWSBin
  reloadenv
  ls $WSBIN_DIR &> /dev/null && echo "There are available bits at $WSBIN_DIR..."
}

function tagstuff {
  jumpBack=false
  if [ "$1" != "." ]; then
    if [ "$1" != "" ]; then
      cd $1
      jumpBack=true
    else
      if [ "$cwp" != "" ]; then
        cd $PROJECT_DIR/$cwp'_proj'
        jumpBack=true
      fi
    fi
  fi

  ctags . && find . -regex ".*\.[cht][dp]*" > cscope.files && cscope -b

  if [ "$jumpBack" == true ]; then
    cd -
  fi
}

function tagstuffhere {
  ctags . && find . -regex ".*\.[cht][dp]*" > cscope.files && cscope -b
}


function tagstuffhere_llvm {
  ctags lib include
  find lib -regex ".*\.[cht][dp]*" > cscope.files
  find include -regex ".*\.[cht][dp]*" >> cscope.files
  cscope -b
}


function tagstuffhere_llvm_tools {
  ctags lib include tools utils
  cp tags .tags
  find lib -regex ".*\.[cht][dp]*" > cscope.files
  find include -regex ".*\.[cht][dp]*" >> cscope.files
  find tools -regex ".*\.[cht][dp]*" >> cscope.files
  find utils -regex ".*\.[cht][dp]*" >> cscope.files
  cscope -b
}

# Change workspace
function cw {
  if [ "$1" == "" ]; then
    echo "Select a workspace:"
    listws
  else
    export cwp=$1
    cd $PROJECT_DIR/$cwp'_proj'
    UpdatePathWithWSBin
    reloadenv
  fi
}

function jumpws {
  if [ "$cwp" != "" ]; then
    BASE_DIR=$PROJECT_DIR
    if [ "$1" == "build" ]; then
      BASE_DIR=$BUILD_DIR
    fi
    cd $BASE_DIR/$cwp'_proj'
  else
    echo "No workspace selected."
  fi
}

function showws {
if [ "$cwp" != "" ]; then
    if [ "$1" != "full" ]; then
        echo "Current project workspace is $cwp."
    else
        echo "Current project workspace is $PROJECT_DIR/$cwp"'_proj'
    fi
else
    echo "No workspace selected."
fi
}

function buildws {
  if [ "$cwp" != "" ]; then
    BUILD_DIR_cwp=$BUILD_DIR/$cwp'_build'
    mkdir -p $BUILD_DIR_cwp

    BUILD_SUBDIR_NAME=$CMAKE_BUILD_TYPE
    if [ "$CMAKE_USE_ASAN" == "true" ]; then
      BUILD_SUBDIR_NAME="ASAN_"$CMAKE_BUILD_TYPE
    fi

    if [ ! -e build ]; then
      ln -s $BUILD_DIR_cwp/$BUILD_SUBDIR_NAME build
    fi

    cd $BUILD_DIR_cwp
    pwd

    mkdir -p $BUILD_SUBDIR_NAME
    cd $BUILD_SUBDIR_NAME

    if [ "$1" == "clobber" ]; then
      cd $PROJECT_DIR/$cwp'_proj'
      echo "Clobbering $BUILD_DIR/$cwp""_build/$BUILD_SUBDIR_NAME..."
      rm -rf $BUILD_DIR/$cwp'_build'/$BUILD_SUBDIR_NAME
      return 1
    elif [ "$1" == "32bitcpu" ]; then
      echo "Running CMAKE..."
      time cmake -G "$BUILD_SYS_TYPE" -DLLVM_BUILD_32_BITS=ON -DCMAKE_BUILD_TYPE=$CMAKE_BUILD_TYPE \
        -DLLVM_TARGETS_TO_BUILD="X86" -DLLVM_ENABLE_LIBCXX=true $PROJECT_DIR/$cwp'_proj'
    elif [ -e "Makefile" ]; then
      echo "Makefile exists, no need for CMake."
    elif [ -e "build.ninja" ]; then
      echo "Ninja file exists, no need for CMake."
    else

      if [ "$BUILD_TARGETS" != "" ]; then
        CMAKE_BUILD_TYPE_FLAG=-DLLVM_TARGETS_TO_BUILD=$BUILD_TARGETS
      fi

      if [ "$FASTER_LLVM_TBLGEN" != "" ]; then
        CMAKE_LLVM_TABLEGEN_LOC=-DLLVM_TABLEGEN=$FASTER_LLVM_TBLGEN
      fi

      echo "Running CMAKE..."
      if [ "$CMAKE_USE_ASAN" == "true" ]; then
        time cmake -G "$BUILD_SYS_TYPE" -DCMAKE_BUILD_TYPE=$CMAKE_BUILD_TYPE \
          -DLLVM_USE_SANITIZER=Address \
          -DLLVM_ENABLE_ASSERTIONS=$ENABLE_ASSERTIONS -DLLVM_ENABLE_LIBCXX=true \
          $CMAKE_LLVM_TABLEGEN_LOC \
          $CMAKE_BUILD_TYPE_FLAG $PROJECT_DIR/$cwp'_proj'
      else
        time cmake -G "$BUILD_SYS_TYPE" -DCMAKE_BUILD_TYPE=$CMAKE_BUILD_TYPE \
          -DLLVM_ENABLE_ASSERTIONS=$ENABLE_ASSERTIONS -DLLVM_ENABLE_LIBCXX=true \
          $CMAKE_LLVM_TABLEGEN_LOC \
          $CMAKE_BUILD_TYPE_FLAG $PROJECT_DIR/$cwp'_proj'
      fi
    fi

    if [ "$1" == "check" ]; then
      echo "Doing a $BUILD_SYS_CMD check on ws in `pwd`."
      time $BUILD_SYS_CMD check
      tmpmsg="Finished $BUILD_SYS_CMD checking project"
    elif [ "$1" == "check-all" ]; then
      echo "Doing a $BUILD_SYS_CMD check-all on ws in `pwd`."
      time $BUILD_SYS_CMD check-all
      tmpmsg="Finished $BUILD_SYS_CMD checking-all project."
    elif [ "$1" == "check-asan" ]; then
      echo "Doing a $BUILD_SYS_CMD check-asan on ws in `pwd`."
      time $BUILD_SYS_CMD check-asan
      tmpmsg="Finished $BUILD_SYS_CMD checking-all project."
    elif [ "$1" == "clean" ]; then
      echo "Doing a $BUILD_SYS_CMD clean on ws in `pwd`."
      time $BUILD_SYS_CMD clean
      tmpmsg="Finished $BUILD_SYS_CMD cleaning project."
    else
      echo "Building in `pwd`."
      time $BUILD_SYS_CMD
      tmpmsg="Finished building project"
    fi

    UpdatePathWithWSBin
    cd $PROJECT_DIR/$cwp'_proj'

    echo "$tmpmsg $cwp."
    say -v Fred $tmpmsg &
  else
    echo "No workspace selected."
  fi
}

function editenv {
  $EDITOR ~/.bashrc
  reloadenv
}



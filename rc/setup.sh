#!/bin/bash -x
cd configs
CONF_DIR=`pwd`
cd ~
# mv .bash_profile .bashrc .gvimrc .vim .vimrc bin ~/.Trash
# Use hardlinks here for NFS from the device.
ln $CONF_DIR/.bash_profile
ln $CONF_DIR/.bashrc
ln $CONF_DIR/.profile
ln $CONF_DIR/.gitconfig
ln $CONF_DIR/.tmux.conf
ln $CONF_DIR/.gvimrc
ln $CONF_DIR/.vimrc
cp -r $CONF_DIR/.vim ~/.vim
ln -s $CONF_DIR/../../bin
ln -s $CONF_DIR/../../vimwiki
cd - && cd ..

mv ~/.vim/bundle ~/.Trash/
git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
vim -c BundleInstall



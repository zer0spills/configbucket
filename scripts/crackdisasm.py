#!/usr/bin/python

import sys

llFilePrefixPath = sys.argv[1]

with open(sys.argv[1]) as statusf:
  content = statusf.readlines()
  lineidx = 0
  llfileincr = 0
  while lineidx < len(content):
    if content[lineidx].find("driver disassembler) -------") > -1:
      llfullfilename = llFilePrefixPath + "_" +str(llfileincr) + ".disasm"
      llfile = open(llfullfilename, 'w')
      llfileincr = llfileincr + 1
      llfile.write("\n; " + content[lineidx])
      lineidx = lineidx + 1
      while lineidx < len(content):
        lineidx = lineidx + 1
        if content[lineidx-1].find("driver disassembler) -------") > -1:
          llfile.write("; " + content[lineidx-1])
          break
        else:
          llfile.write(content[lineidx-1])
      llfile.close()
    else:
      lineidx = lineidx + 1




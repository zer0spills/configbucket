#!/usr/bin/python

import sys

with open(sys.argv[1]) as statusf:
  content = statusf.readlines()
  lineidx = 0
  while lineidx < len(content):
    lineidx = lineidx + 1

    if content[lineidx-1].find("namespace llvm") > -1:
      continue

    if content[lineidx-1].find("namespace AGX1") > -1:
      continue

    if content[lineidx-1].find("enum") > -1:
      llfile = open("opcodes.inc", 'w')
      llfile.write("#ifndef _AGX1Opcodes_\n")
      llfile.write("#define _AGX1Opcodes_\n")
      llfile.write("static const char* AGX1Opcodes [] = {\n")

      while lineidx < len(content):
        lineidx = lineidx + 1
        llfile.write("    AGX1::" + content[lineidx-1].split('=')[0].strip() + ",\n")
        if content[lineidx].find("INSTRUCTION_LIST_END") > -1:
          llfile.write("    AGX1::" + content[lineidx].split('=')[0].strip() + "\n};\n")
          llfile.write("#endif /* _AGX1Opcodes_ */\n")
          llfile.close()
          sys.exit()



# Dark Mode
defaults write "Apple Global Domain"  AppleInterfaceStyle Dark

# keyboard repeat rate
defaults write NSGlobalDomain KeyRepeat -int 2
defaults write NSGlobalDomain InitialKeyRepeat -int 15

# Disable press-and-hold for keys in favor of key repeat
# defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false

# Enable full keyboard access for all controls (e.g. enable Tab in modal dialogs)
defaults write NSGlobalDomain AppleKeyboardUIMode -int 3

# tap to click on Trackpad
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
defaults write com.apple.AppleMultitouchTrackpad Clicking -bool true

# window dragging
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Dragging -bool true
defaults write com.apple.AppleMultitouchTrackpad Dragging -bool true

# Require password immediately after sleep or screen saver begins
defaults write com.apple.screensaver askForPassword -int 1
defaults write com.apple.screensaver askForPasswordDelay -int 0

# safari
defaults write com.apple.Safari ShowStatusBar -bool true
defaults write com.apple.Safari IncludeDevelopMenu -bool true
defaults write com.apple.Safari "ShowFavoritesBar-v2" 1

# dock orientation and minimize effect
defaults write com.apple.dock orientation right
defaults write com.apple.dock mineffect scale

# don't auto-rearrange spaces
defaults write com.apple.dock mru-spaces -bool false

# don't group windows by application
defaults write com.apple.dock expose-group-by-app -bool false

# Automatically hide and show the Dock, and Remove delay with auto-hide
# defaults write com.apple.dock autohide -bool true
# defaults write com.apple.dock autohide-delay 0

# Disable dashboard
defaults write com.apple.dashboard enabled-state -int 1

# Show icons for hard drives, servers, and removable media on the desktop
defaults write com.apple.finder ShowExternalHardDrivesOnDesktop -bool true
defaults write com.apple.finder ShowHardDrivesOnDesktop -bool true
defaults write com.apple.finder ShowMountedServersOnDesktop -bool true
defaults write com.apple.finder ShowRemovableMediaOnDesktop -bool true

# Finder: show hidden files by default
defaults write com.apple.Finder AppleShowAllFiles -bool false
defaults write com.apple.Finder AppleShowAllExtensions -bool true

# Enable AirDrop over Ethernet
# defaults write com.apple.NetworkBrowser BrowseAllInterfaces 1 

# Show remaining battery time and percentage
defaults write com.apple.menuextra.battery ShowPercent -string "YES"

# echo "Always show scrollbars"
# defaults write NSGlobalDomain AppleShowScrollBars -string "Auto"

# Disable dashboard, because it totally sucks and this isn't 2005
defaults write com.apple.dashboard mcx-disabled -boolean true

#Terminal Settings
#defaults write com.apple.Terminal "Startup Window Settings" Pro
#defaults write com.apple.Terminal "Default Window Settings" Pro

# Make the volume buttons make sound.
defaults write "Apple Global Domain" com.apple.sound.beep.feedback  -int 1
